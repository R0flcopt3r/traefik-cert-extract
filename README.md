# traefik-cert-extract

Go program for extracting certificates from acme.json

## Usage

```
Usage of traefik-cert-extract:
  -domain string
    	Optional: The full domain	example.com,test.example.com
    		Omitting this will output every certificate
  -input string
    	Optional: Input json file	acme.json (default "acme.json")
  -output string
    	Optional: Output file	cert.pem
  -resolver string
    	Required: Resolver of your domain	example
```
### examples:

**Printing to stdout**
```
traefik-cert-extract -resolver example -domain test.example.com > test.crt
```

**Output to specified file**
```
traefik-cert-extract -resolver example -domain test.example.com -output test.crt
```

**Extract every certificate**
```
traefik-cert-extract -resolver example
```

### Example

**Example json file:**
``` json
{
	"example": {
		"Account": {
			"Email": "mail@example.com",
			"Registration": {
				"body": {
					"status": "valid",
					"contact": [
						"mailto:mail@example.com"
					]
				},
				"uri": "https://acme-v02.api.letsencrypt.org/acme/acct/727272"
			},
			"PrivateKey": "THi51SAPriV4T3K3y",
			"KeyType": "4096"
		},
		"Certificates" : [
			{
				"domain": {
					"main": "test.example.com"
				},
				"certificate": "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUpSQUlCQURBTkJna3Foa2lHOXcwQkFRRUZBQVNDQ1M0d2dna3FBZ0VBQW9JQ0FRRElRY1ArNkUyeTBNK2sKYnJsN1dXbDdhTmtlRm8yREcyN2NlWXAyYjIvK1UrbHNrZUNCODk0ell0MC9DY21FRmtxbHY3VlhLMlBieTdoSAprWStmVTdveVB4d1FCaGdwQjNHZC9RQjdKcEFITUp3a0ttVDV5OG51K1BJcXBXNWptQjhCYVFXVHhyTlgvYzlzCk1JSUZXekNDQTBPZ0F3SUJBZ0lKQUpMckROaXhURzQrTUEwR0NTcUdTSWIzRFFFQkN3VUFNRVF4Q3pBSkJnTlYKTkY2S2N6bUg5c0RWbURLd2lZTi9IZmc3bkc2dllQVDVWOHdjdW41eEtadURCTm1pcTlrdGI1cUttMEh5OE1FeApNSUlGV3pDQ0EwT2dBd0lCQWdJSkFKTHJETml4VEc0K01BMEdDU3FHU0k9PQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0t",
				"key": "LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tCjN0dUE5VUtnUzRtWVdiQXcxdUI5RWhKV2RuU20xUU52dm5xK1Q5dk1pMkVjb2FDcExaRmZmZjJSYjRCRnNhdXoKTkY2S2N6bUg5c0RWbURLd2lZTi9IZmc3bkc2dllQVDVWOHdjdW41eEtadURCTm1pcTlrdGI1cUttMEh5OE1FeApKQjJQaW0xOFEzUDE1aVRGTjlvM0FhUVZ2MUZyTnlTcFVvdnYreHBaUnBmckJlbFJ6d3JCWmE1U3JQanNGSnd0CnBKejIxRHo3bUdPWTl2dy95VWx2d2EzUktHRk5aOFp6TDRJRFBBVyt1YU5PcU8ydTcxMzZyVWxIb2QxaTJvamMKWVZ1YURJeDhFK3ZzYzYvNzJEcDJBL2ZEV2QxMVlQR3B0aHdLdTcxeXA3aTZIY2JSTGl0Zk9VUkpld1ZWSmdkQQpwSnoyMUR6N21HT1k5dncveVVsdndhM1JLR0ZOWjhaegotLS0tLUVORCBQUklWQVRFIEtFWS0tLS0tCgo=",
				"Store": "default"
			}
		]
	}
}
```

**usage:**
`traefik-cert-extract -resolver=example -domain=test.example.com -input=example.json`

**result:**
```
-----BEGIN PRIVATE KEY-----
3tuA9UKgS4mYWbAw1uB9EhJWdnSm1QNvvnq+T9vMi2EcoaCpLZFfff2Rb4BFsauz
NF6KczmH9sDVmDKwiYN/Hfg7nG6vYPT5V8wcun5xKZuDBNmiq9ktb5qKm0Hy8MEx
JB2Pim18Q3P15iTFN9o3AaQVv1FrNySpUovv+xpZRpfrBelRzwrBZa5SrPjsFJwt
pJz21Dz7mGOY9vw/yUlvwa3RKGFNZ8ZzL4IDPAW+uaNOqO2u7136rUlHod1i2ojc
YVuaDIx8E+vsc6/72Dp2A/fDWd11YPGpthwKu71yp7i6HcbRLitfOURJewVVJgdA
pJz21Dz7mGOY9vw/yUlvwa3RKGFNZ8Zz
-----END PRIVATE KEY-----


-----BEGIN CERTIFICATE-----
MIIJRAIBADANBgkqhkiG9w0BAQEFAASCCS4wggkqAgEAAoICAQDIQcP+6E2y0M+k
brl7WWl7aNkeFo2DG27ceYp2b2/+U+lskeCB894zYt0/CcmEFkqlv7VXK2Pby7hH
kY+fU7oyPxwQBhgpB3Gd/QB7JpAHMJwkKmT5y8nu+PIqpW5jmB8BaQWTxrNX/c9s
MIIFWzCCA0OgAwIBAgIJAJLrDNixTG4+MA0GCSqGSIb3DQEBCwUAMEQxCzAJBgNV
NF6KczmH9sDVmDKwiYN/Hfg7nG6vYPT5V8wcun5xKZuDBNmiq9ktb5qKm0Hy8MEx
MIIFWzCCA0OgAwIBAgIJAJLrDNixTG4+MA0GCSqGSI==
-----END CERTIFICATE-----%
```
